<?php

	function just_share_admin_settings() {
		$form['just_share_links'] = array(
			'#type'          => 'checkboxes',
			'#title'         => t('Links to show'),
			'#description'   => t('Facebook, Twitter o both?'),
			'#default_value' => variable_get('just_share_links', array()),
			'#options'       => array('facebook' => t('Facebook'), 'twitter' => t('Twitter')),
			'#default_value' => variable_get('just_share_links', array('facebook', 'twitter')),
		);
		
		$form['just_share_twitter_account'] = array(
			'#type'          => 'textfield',
			'#size'			 => 20,
			'#description'   => t('A twitter account to suggest at the end of the users\'s tweet. Tipically, site\'s owner twitter account. No "@" please.'),
			'#title'         => t('Twitter account'),
			'#default_value' => variable_get('just_share_twitter_account', ''),
		);
		
		$form['just_share_twitter_language'] = array(
			'#type'          => 'textfield',
			'#size'			 => 1,
			'#description'   => t('2 letter language code.'),
			'#title'         => t('Language for the Twitter link'),
			'#default_value' => variable_get('just_share_twitter_language', 'en'),
		);
		
		$form['just_share_twitter_layout'] = array(
			'#type'          => 'radios',
			'#title'         => t('Facebook link layout'),
			'#description'   => t('Will the tweet count appear at the vertically, horizontally or won\'t appear at all?'),
			'#options'       => array('vertical' => t('Vertically'), 'horizontal' => t('Horizontally'), 'none' => t('No count'), ),
			'#default_value' => variable_get('just_share_twitter_layout', 'horizontal'),
		);

		$form['just_share_facebook_layout'] = array(
			'#type'          => 'radios',
			'#title'         => t('Facebook link layout'),
			'#description'   => t('Will the tweet count appear at the vertically, horizontally?'),
			'#options'       => array('box_count' => t('Vertically'), 'button_count' => t('Horizontally'), ),
			'#default_value' => variable_get('just_share_facebook_layout', 'button_count'),
		);
		
		$form['just_share_facebook_size'] = array(
			'#type'          => 'textfield',
			'#size'			 => 5,
			'#title'         => t('Facebook link size'),
			'#description'   => t('Size of the Facebook link size in pixels'),
			'#default_value' => variable_get('just_share_facebook_size', '90'),
		);
		
		$form['just_share_weight'] = array(
			'#type'          => 'weight',
			'#delta'         => 10,
			'#description'   => t('Where you want the share links to appear in the node. Negative numbers are rendered earlier, positive numbers are rendered later.'),
			'#title'         => t('Weight'),
			'#default_value' => variable_get('just_share_weight', 10),
	  );

	  return system_settings_form($form);
	}